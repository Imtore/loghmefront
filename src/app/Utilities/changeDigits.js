function changeDigits(digits)
{
    var persianDigits = ""
    for (var i = 0; i < digits.toString().length; i++) {
        switch (digits.toString()[i]) {
            case '0':
                persianDigits+='۰';
                break;
            case '1':
                persianDigits+='۱';
                break;
            case '2':
                persianDigits+='۲';
                break;
            case '3':
                persianDigits+='۳';
                break;
            case '4':
                persianDigits+='۴';
                break;
            case '5':
                persianDigits+='۵';
                break;
            case '6':
                persianDigits+='۶';
                break;
            case '7':
                persianDigits+='۷';
                break;
            case '8':
                persianDigits+='۸';
                break;
            case '9':
                persianDigits+='۹';
                break;
        }
    }
    return persianDigits
}

export default changeDigits