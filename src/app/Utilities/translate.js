function translate(text) {
    const persianNumbers = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹']
    if (typeof text === 'string') {
        let chars = text.split('');
        for (let i = 0; i < chars.length; i++) {
            if (/\d/.test(chars[i])) {
                chars[i] = persianNumbers[chars[i]];
            }
        }

        return chars.join('');
    }
    return null;
}

export default translate