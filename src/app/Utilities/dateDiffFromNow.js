import moment from "moment";

function dateDiffFromNow(end){
    const startDate = moment();
    const timeEnd = moment(end);
    const diff = timeEnd.diff(startDate);
    const diffDuration = moment.duration(diff);
    return diffDuration;
}

export default dateDiffFromNow