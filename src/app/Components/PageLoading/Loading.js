import React from 'react';
import './Loading.css'



class Loading extends React.Component
{
    render() {
        return(
        <div className="modal loading-modal">
            <div className="loading-modal-content">
                <div className="d-flex justify-content-center">
                    <div className="spinner-border" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                </div>
            </div>
        </div>
        )}

}


export default Loading;