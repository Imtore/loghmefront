import React from 'react';
import './Footer.css';

function Footer(){
    return(
        <footer className="container-fluid footer" >
            <div className="row">
                <div className="col" id="footer-col">
                    <p className="footer">© تمامی حقوق متعلق به لقمه است.</p>
                </div>
            </div>
        </footer>
    );
}

export default Footer