import {Redirect, Route} from "react-router-dom";
import React from "react";
import AuthService from './../Services/auth.service'

export default function PrivateRoute({component: Component, data, ...rest}){
    return(<Route {...rest} render={props => (
                AuthService.getCurrentUser()
                ?
                (<Component {...props} {...data}/>)
                :
                (<Redirect to={{pathname: '/login', state: {from: props.location}}}/>)
        )}/>
    );
}
