import React from 'react';




class ButtonLoading extends React.Component
{
    constructor(props) {
        super(props);
    }
    render() {
        return(
            <button className={"btn btn-primary " + this.props.className} type="button" disabled>
                                <span className="spinner-border spinner-border-sm" role="status"
                                      aria-hidden="true"></span>
                <span className="sr-only">Loading...</span>
            </button>
        )}

}


export default ButtonLoading;