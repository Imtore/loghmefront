import React from "react";
import './Cart.css'
import '../../../assets/iconfont/flaticon.css'
import translate from "../../Utilities/translate";
import ButtonLoading from "../ButtonLoading/ButtonLoading";


class Cart extends React.Component{
    constructor(props) {
        super(props);
        this.state = {cartIsLoading:props.cartIsLoading}
    }

    componentWillReceiveProps(nextProps) {
        // alert("willrecieve:" + nextProps.cartIsLoading)
        if (nextProps.cartIsLoading !== this.props.cartIsLoading) {
            this.setState({cartIsLoading: nextProps.cartIsLoading});
        }
    }
    render() {
        const itemCounts = this.props.info.cartItems;
        const itemPrices = this.props.info.cartItemPrices;
        let items;
        if(!(this.props.info.empty===null) && !this.props.info.empty){
            items = Object.keys(itemCounts).map((item, index)=>{
                const name = item;
                const count = itemCounts[name];
                const price = itemPrices[name];
                return (
                    <div className="food-item crt" key={index}>
                        <div className="crt-item-first-line">
                            <span className="crt-food-title">{name}</span>
                            <span className="flaticon-plus crt"  onClick={()=>{this.props.onAddClick({name:name, price:price, restaurantId:this.props.info.restaurantId, count:1})}}></span>
                            <p className="crt-food-count">{translate(count.toString())}</p>
                            <i className="flaticon-minus crt" onClick={()=>{this.props.onAddClick({name:name, price:price, restaurantId:this.props.info.restaurantId, count:-1})}}></i>
                        </div>
                        <span className="crt-food-price">{translate(price.toString())} تومان</span>
                        <hr className="crt-line"/>
                    </div>
                );
            });
        }

        const content = ()=>{
            if(this.props.info.empty || this.props.info.empty===null){
                return(
                    <p>سبد خرید شما خالی است.</p>
                );
            }
            let total = this.props.info.total;
            if(total){
                total = translate(total.toString());
            }
            return (
               <div>
                   <div className="food-list crt ">
                       {items}
                   </div>
                   <div className="crt-total-sum">
                       <p id="crt-total">جمع کل: </p>
                       <p id="crt-sum"> {total} تومان</p>
                   </div>
                   {this.state.cartIsLoading? <ButtonLoading className="cart-finalize-disabled"/>:
                       <button type="button" className="btn btn-info crt-submit-order" onClick={() => {
                           this.props.onSubmitClick()
                       }}>تایید نهایی</button>
                   }
               </div>
            );
        }

        return(
            <div className="card text-center" id="crt-card">
                <div className="card-body crt-card-body">
                    <p id="crt-cart-title">سبد خرید</p>
                    {content()}
                </div>
            </div>
        );
    }
}

export default Cart