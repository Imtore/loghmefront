import React from 'react'
import './Header.css'
import logo from '../../../assets/LOGO.png'
import translate from "../../Utilities/translate";
import CartModal from "./Components/CartModal/CartModal";
import {Link, Redirect} from "react-router-dom";
import AuthService from './../../Services/auth.service'
import GoogleLogout from "react-google-login";


class Header extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            seen:false,
            cartIsLoading:props.cartIsLoading,
            redirectToLogin: false
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.cartIsLoading !== this.props.cartIsLoading) {
            this.setState({cartIsLoading: nextProps.cartIsLoading});
        }
    }

    logout(event){
        console.log("HEREEEEE")
        AuthService.logout()
        this.setState({
                redirectToLogin: true
            })
    }

    togglePop(event){
        if(event.target.id==="ic1"){
            this.setState({
                seen: true
            });
        }else if(event.target.id==="md1"){
            this.setState({
                seen: false
            });
        }
    };

    render(){
        if(this.state.redirectToLogin){
            return (
                <Redirect to={'/login'}/>
            );
        }
        let count = 0;
        if(!(this.props.cart.empty===null) && !this.props.cart.empty){
            let itemNames = Object.keys(this.props.cart.cartItems);
            itemNames.forEach((name, index)=>{count += this.props.cart.cartItems[name]});
        }
        return (
            <header className="container-fluid">
                {this.props.exitLink ?
                     <a className="hdr-exit-link" href='#' onClick={(e)=>{this.logout(e)}} >خروج</a>: null}
                {this.props.profileLink ? <Link className="hdr-account" to='/profile' >حساب کاربری</Link> : null}
                <i className="flaticon-smart-cart" id="ic1" onClick={(event)=>{this.togglePop(event)}}></i>
                {this.state.seen ? <CartModal cartIsLoading={this.state.cartIsLoading} cart={this.props.cart} toggle={(event)=>this.togglePop(event)} onAddClick={(info)=>{this.props.onAddClick(info)}}
                               onSubmitClick={()=>{
                                   this.setState({seen:false});
                                   this.props.onSubmitClick();
                               }}/>
                               : null}
                <span className="counter counter-lg hdr">{translate(count.toString())}</span>
                <Link to='/'>
                    <img className="hdr-logo" alt="logo" src={logo}/>
                </Link>

            </header>
        );
    }
}


export default Header;