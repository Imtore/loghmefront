import React from 'react';
import Cart from "../../../Cart/Cart";
import './CartModal.css'


class CartModal extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            cartIsLoading:props.cartIsLoading
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.cartIsLoading !== this.props.cartIsLoading) {
            this.setState({cartIsLoading: nextProps.cartIsLoading});
        }
    }
    render(){
        return(
            <div className="modal cart-modal" id="md1" onClick={this.props.toggle}>
                <div className="modal-content cart-modal">
                    <div className="row" id="hdr-modal-row" >
                        <div className="col-md-11 col-lg-9 cart-modal">
                            <Cart cartIsLoading ={this.state.cartIsLoading} info={this.props.cart} onAddClick={(info)=>{this.props.onAddClick(info)}} onSubmitClick={()=>{this.props.onSubmitClick()}}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CartModal