import axios from "axios";


class AuthService {

    login(email, password) {
        return axios
            .post('auth/login', {
                email: email,
                password: password
            })
            .then(response => {
                console.log(response.data)
                // if (response.data.accessToken) {
                    localStorage.setItem("user", JSON.stringify(response.data));
                    localStorage.setItem("google", JSON.stringify(false));
                // }

                return response.data;
            });
    }

    googleLogin(token) {
        return axios.post('auth/google',{
            token: token
        })
            .then((response) => {
                console.log(response.data)
                localStorage.setItem("user", JSON.stringify(response.data));
                localStorage.setItem("google", JSON.stringify(true));
                return response.data;
            });
    }

    logout() {
        localStorage.removeItem("user");
        localStorage.removeItem("google");
        localStorage.clear()
        sessionStorage.clear()
    }

    register(firstName, lastName, phoneNumber, email, password) {
        return axios.post('auth/signup', {
            firstName: firstName, lastName: lastName, phoneNumber: phoneNumber, email: email, password: password
        });
    }

    getCurrentUser() {
        return JSON.parse(localStorage.getItem('user'));
    }

    getGoogleClientId(){
        return "587251128426-ca6j5e1e0cbuq9cjtm1gqriqvsv0kich.apps.googleusercontent.com"
    }

    hasSignedUpWithGoogle(){
        return JSON.parse(localStorage.getItem('google'));
    }
}

export default new AuthService();