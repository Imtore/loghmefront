import React from 'react';
import Home from "./Scenes/Home/Home";
import Restaurant from "./Scenes/Restaurant/Restaurant";
import Login from "./Scenes/Login/Login";
import SignUp from "./Scenes/SignUp/SignUp";
import Profile from "./Scenes/Profile/Profile";
import axios from "axios";
import {Switch} from "react-router";
import {Redirect, Route} from "react-router-dom";
import PrivateRoute from './Components/PrivateRoute'
import AuthService from './Services/auth.service'
import authHeader from "./Services/authHeader";

class App extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            cart: {
                restaurantId : null,
                restaurantName : null,
                cartItems: {},
                cartItemPrices : {},
                empty: null,
                total: null
            },
            userId : "1",
            cartIsLoading :false
        }
    }

    addToCart(info){
        this.setState({cartIsLoading:true});
        console.log("reached");
        if(info.count>=1){
            axios.put('users/cart',{foodName:info.name, foodPrice:info.price, restaurantId:info.restaurantId, count:info.count}, {headers: authHeader()})
                .then((response)=> {
                    const cart = response.data;
                    this.setState({cart: cart, cartIsLoading:false});
                    // console.log(response);
                })
                .catch((error) =>{
                    // TODO: an error page for 400 that shows the message from back
                    // console.log(error.message.message);
                    alert(error.response.data.message);
                    this.setState({cartIsLoading:false})
                })
                .then(function () {
                    //TODO: read about this
                    //always executed
                });
        }else if(info.count===-1){
            axios.delete('users/cart',{data:{foodName:info.name, foodPrice:info.price, restaurantId:info.restaurantId, count:-1}, headers: authHeader()})
                .then((response)=> {
                    const cart = response.data;
                    this.setState({cart: cart, cartIsLoading:false});
                    // console.log(response);
                })
                .catch( (error) =>{
                    // TODO: an error page for 400 that shows the message from back
                    alert(error.response.data.message);
                    this.setState({cartIsLoading:false})
                    // console.log(error);
                })
                .then(function () {
                    //TODO: read about this
                    //always executed
                });
        }
    }

    submitOrder(){
        this.setState({cartIsLoading:true});
        axios.post('users/orders',{},{headers: authHeader()})
            .then((response)=> {
                const order = response.data;
                this.setState({cart: {
                        restaurantId : null,
                        restaurantName : null,
                        cartItems: {},
                        cartItemPrices : {},
                        empty: null,
                        total: null
                    }, cartIsLoading:false});
                console.log(order);
            })
            .catch( (error) =>{
                // TODO: an error page for 400 that shows the message from back
                alert(error.response.data.message);
                this.setState({cartIsLoading:false})
                // console.log(error.response.data.message);
            })
            .then(function () {
                //TODO: read about this
                //always executed
            });
    }

    getCartInfo(){
        this.setState({cartIsLoading:true});
        axios.get('users/cart', {headers: authHeader()})
            .then((response)=> {
                const cart = response.data;
                this.setState({cart: cart , cartIsLoading:false});
                // console.log(response);
            })
            .catch( (error)=> {
                // console.log(error);
                alert(error.response.data.message);
                this.setState({cartIsLoading:false})
            })
            .then(function () {
            });
    }

    render(){
        const data = {
            cart: this.state.cart,
            setCart: ()=>{this.getCartInfo()},
            cartIsLoading: this.state.cartIsLoading,
            onAddClick: (info)=>{this.addToCart(info)},
            onSubmitClick: ()=>{this.submitOrder()}
        }

        return(
        <div id="app">
                <Switch>
                    <PrivateRoute
                        path='/profile'
                        component={Profile}
                        data={data}
                    />
                    <Route
                        exact path='/login'
                        render={props => <Login {...props} data={{cartIsLoading: this.state.cartIsLoading}}/>}
                    />
                    <Route
                        exact path='/signup'
                        render={props => <SignUp {...props} data={{cartIsLoading: this.state.cartIsLoading}}/>}
                    />
                    <PrivateRoute
                        path='/restaurant/:id'
                        component={Restaurant}
                        data={data}
                    />
                    <PrivateRoute
                        exact
                        path='/'
                        component={Home}
                        data={data}
                    />
                    <Route>
                        404 not found
                    </Route>
                </Switch>
            </div>
        );
    }
}

export default App