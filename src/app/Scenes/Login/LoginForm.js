import React from "react";
import AuthService from './../../Services/auth.service'
import GoogleLogin from 'react-google-login'
import axios from 'axios';
import {Link, Redirect} from "react-router-dom";

class LoginForm extends React.Component
{
    constructor(props) {
        super(props);
        this.state = {
            Email: '',
            Password: '',
            redirectForRegister: false
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = ({ target }) => {
        this.setState({ [target.name]: target.value });
    };

    handleSubmit(event) {
        event.preventDefault();
        if(this.state.Email !== '' && this.state.Password !=='') {
            AuthService.login(this.state.Email, this.state.Password).then(
                () => {
                    alert("You're logged in");
                    this.props.redirect()
                },
                error => {
                    alert(error.response.data.message);
                }
            );
        }
        else
        {
            alert('Both fields are essential');
            this.setState(prevState =>({Email:"", Password: ""}));
        }
    }

    handleGoogleLogin(res){
        if(res.googleId){
            AuthService.googleLogin(res.tokenId).then(
                ()=>{
                    alert("You're logged in");
                    this.props.redirect()
                },
                error =>{
                    if(error.response.status===403){
                        this.setState({redirectForRegister: true})
                    }
                    alert(error.response.data.message);
                }
            )
        }else
        {
            alert(res);
        }

    }

    render()
    {
        if(this.state.redirectForRegister){
            return (
                <Redirect to={'/signup'}/>
            )
        }

        return(
            <form onSubmit={this.handleSubmit} className='login-form'>
                <div className="login-form-group">
                    <label htmlFor="inputEmail" className='login-label'>ایمیل</label>
                    <input type="email" name='Email' value={this.state.Email} onChange={this.handleChange} className="form-control login-input"  placeholder="ایمیل" />
                </div>
                <div className="login-form-group col-md-7 login-password">
                    <label htmlFor="inputPassword1" className='login-label'>رمز عبور</label>
                    <input value={this.state.Password} name="Password" onChange={this.handleChange} type="password" className="form-control login-input"  placeholder="رمز عبور" />
                </div>
                <GoogleLogin
                    clientId={AuthService.getGoogleClientId()}
                    buttonText="Login with Google"
                    onSuccess={(res)=>{this.handleGoogleLogin(res)}}
                    onFailure={(res)=>{this.handleGoogleLogin(res)}} ></GoogleLogin>
                <button type="submit" className="btn btn-info" id="login-confirm">ورود</button>
                <Link to='/signup' >ثبت نام</Link>
            </form>
        );
    }
}



export default LoginForm;