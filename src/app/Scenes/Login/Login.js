import React from "react";
import './Login.css';
import Logo from '../../../assets/LOGO.png';
import LoginForm from './LoginForm'
import {Redirect} from 'react-router-dom';
import AuthService from './../../Services/auth.service'


class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            redirectToReferrer: false
        }
    }

    redirect(){
        this.setState({
            redirectToReferrer: true
        })
    }

    render()
    {
        const { from } = this.props.location.state || {from: {pathname: '/'}};
        const { redirectToReferrer } = this.state;

        if (redirectToReferrer) {
            return (
                <Redirect to={from}/>
            )
        }
        if(AuthService.getCurrentUser()){
            return (
                <Redirect to={'/'}/>
            )
        }

        return (
            <div className="container-fluid" id='login-page'>
                <WelcomeRow />
                <div className="row" id="login-main-row">
                    <div className="col-md-5 col-lg-4" id="login-main-col">
                       <LoginForm redirect={()=>{this.redirect()}}/>
                    </div>
                </div>
            </div>
        );
    }
}


function WelcomeRow() {
    return(
        <div className="row" id="login-welcome-row">
            <div className="col-md-6 col-lg-5" id="login-welcome-col">
                <img className="login-logo" src={Logo}/>
                <p id="login-description">اولین و بزرگترین سامانه سفارش غذا در دانشگاه تهران</p>
            </div>
        </div>
    );
}



export default Login;





