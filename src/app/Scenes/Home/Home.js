import React from 'react'
import Info from "./Components/Info/Info";
import './Home.css'
import axios from "axios";
import Header from "../../Components/Header/Header";
import RestaurantCard from "./Components/RestaurantCard/RestaurantCard";
import SearchBox from "./Components/SearchBox/SearchBox";
import CountdownTimer from "./Components/CountdownTimer/CountdownTimer";
import dateDiffFromNow from "../../Utilities/dateDiffFromNow";
import ReactDOM from 'react-dom'
import Loading from "../../Components/PageLoading/Loading";
import FoodPartyScrollBox from "./Components/FoodPartyScrollBox/FoodPartyScrollBox";
import authHeader from "../../Services/authHeader";
import moment from 'moment-timezone'


function FoodPartyHeader(){
    return(
        <div className="row home-sec-title-row" id="foodParty">
            <div className="col-md-4 col-lg-3 home-sec-title-col">
                <span className="home-sec-title">جشن غذا!</span>
            </div>
        </div>
    );
}

function RestaurantsHeader(){
    return(
        <div className="row home-sec-title-row" id="restaurant">
            <div className="col-md-4 col-lg-3 home-sec-title-col">
                <span className="home-sec-title">رستوران‌ها</span>
            </div>
        </div>
    );
}

class Home extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            restaurants : [{
                restaurant: {
                    id: null,
                    name: null,
                    description: null,
                    location: {
                        x: null,
                        y: null
                    },
                    menu: [{
                        food: {
                            name: null,
                            description: null,
                            popularity: null,
                            price: null,
                            imageURL: null
                        }
                    }
                    ],
                    foodPartyItems: [],
                    logoURL: null,
                    inFoodParty: null
                }
            }],
            partyFoods : null,
            pageIsLoading: false,
            timeOut: false,
            cartIsLoading: props.cartIsLoading,
            searching: false,
            offset: 0,
            count: 30
        }
    }

    componentWillReceiveProps(nextProps){
        // alert("willrecieve:" + nextProps.cartIsLoading)
        this.getFoodPartyInfo(null)
        this.setState({searching:false})
        if(nextProps.cartIsLoading !== this.props.cartIsLoading){
            this.setState({cartIsLoading :nextProps.cartIsLoading});
        }
    }


    getRestaurantsInfo(){
        axios.get('restaurants?offset=0&count='+this.state.count, {headers: authHeader()})
            .then((response)=> {
                const restaurants = response.data;
                this.setState({restaurants:restaurants, offset: this.state.offset+this.state.count});
                // console.log(response);
            })
            .catch(function (error) {
                //an error page for 400 that shows the message from back
                console.log(error);
            })
            .then(function () {
                //always executed
            });
    }

    //TODO : add authHeader if needed
    getFoodPartyInfo(event){
        this.setState({pageIsLoading:true})
        if(event){
            event.preventDefault();
        }
        axios.get('foodParty',{headers: authHeader()}).then((response) => {
            this.setState({pageIsLoading:false})
            const foodParty =  response.data;
            const partyFoods = foodParty.partyFoods;
            const foodPartyDeadline = foodParty.deadline;
            let deadlineDate = new Date(foodPartyDeadline);
            console.log("deadline = " + deadlineDate + "deadline moment: " + moment(deadlineDate.toUTCString()) + "moment: " + moment() + "foodparty lenghth : " + partyFoods.length);
            if(partyFoods.length!==0){

                if(moment.tz(deadlineDate, 'Asia/Tehran') > moment()){
                    const duration = dateDiffFromNow(deadlineDate.toISOString());
                    console.log("duration: "+ duration.minutes() +" "+ duration.seconds());
                    this.setState({partyFoods: partyFoods, timeOut: false})
                    ReactDOM.unmountComponentAtNode(document.getElementById('home-counter'))
                    ReactDOM.render(<CountdownTimer onDone={()=>{this.timerDone()}} minutes={duration.minutes()} seconds={duration.seconds()} />
                        ,document.getElementById('home-counter'));
                }
            }
            this.setState({pageIsLoading:false})


        }).catch(function (error) {
            console.log(error);
        })
            .then(function () {
            });

    }

    updateFoodPartyInfo(){
        console.log("HEREEEEE")
        axios.get('foodParty', {headers:authHeader()})
            .then((response)=> {
                const partyFoods = response.data.partyFoods;
                console.log(partyFoods)
                this.setState({partyFoods:partyFoods});
                // console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            })
            .then(function () {
            });
    }

    componentDidMount() {
        this.props.setCart();
        this.setState({pageIsLoading:true})
        this.getRestaurantsInfo();
        // this.timerId1 = setInterval(
        //     () => {
        //         if(!this.state.searching) {
        //             this.getRestaurantsInfo();
        //         }
        //     }
        //     , 5000
        // );

        this.getFoodPartyInfo(null);
        // this.count()
    }

    componentWillUnmount() {
        clearInterval(this.timerId1);

    }


    timerDone(){
        this.setState({timeOut:true})
    }

    Restaurants(){
        const restaurantsArray = this.state.restaurants;
        const restaurants = restaurantsArray.map((restaurant, index)=>{
            return(
                <RestaurantCard key={index} i={index} info={restaurant} />
            );
        });

        return(
            <div className="row home-restaurants">
                <div className="col-md-10 col-lg-10">
                    <div className="row" id="home-rest-row">
                        {restaurants}
                    </div>
                </div>
            </div>
        );
    }

    doSearch(foodName, restaurantName){
        if(foodName===''){
            foodName = "none"
        }
        if(restaurantName===''){
            restaurantName = "none"
        }

        this.setState({pageIsLoading:true})
        axios.get('restaurants/search?'+'restaurant='+restaurantName+'&'+'food='+foodName, {headers:authHeader()}).then((response) => {
            this.setState({pageIsLoading:false})
            const restaurants = response.data;
            this.setState({restaurants:restaurants, searching:true});
        }).catch(function (error) {
            console.log(error);
        }).then(function () {

        });
    }


    loadMore(){
        this.setState({pageIsLoading:true})
        axios.get('restaurants?'+'offset='+this.state.offset+'&'+'count='+this.state.count,{headers: authHeader()})
            .then((response)=> {
                const restaurants = response.data;
                console.log(restaurants);
                this.setState({restaurants: this.state.restaurants.concat(restaurants), offset: this.state.offset+this.state.count});
                console.log(this.state.restaurants)
                this.setState({pageIsLoading:false})
                // console.log(response);
            })
            .catch(function (error) {
                //an error page for 400 that shows the message from back
                console.log(error);
            })
            .then(function () {
                //always executed
            });
    }


    render(){
        return(
            <div>
                {this.state.pageIsLoading?<Loading />:null}
                <Header cartIsLoading={this.state.cartIsLoading} cart={this.props.cart} exitLink={true} profileLink={true} onAddClick={(info)=>{this.props.onAddClick(info)}} onSubmitClick={()=>{this.props.onSubmitClick()}}/>
                <Info/>
                <SearchBox onSearch={(fn, rn)=>{this.doSearch(fn , rn)}}/>
                {this.state.searching ?
                        this.Restaurants()
                    :
                    <div className="container-fluid home-content">
                        <FoodPartyHeader/>
                        <FoodPartyScrollBox onClickUpdate={() => {
                            this.updateFoodPartyInfo()
                        }} timeOut={this.state.timeOut} cartIsLoading={this.state.cartIsLoading} refresh={(event) => {
                            this.getFoodPartyInfo(event)
                        }} partyFoods={this.state.partyFoods} onAddClick={(info) => {
                            this.props.onAddClick(info)
                        }}/>
                        <RestaurantsHeader/>
                        {this.Restaurants()}
                        <button type="button" className="btn btn-info home-sb-button" onClick={()=>{this.loadMore()}}>بیشتر</button>
                    </div>
                }
            </div>


        );
    }
}

export default Home
