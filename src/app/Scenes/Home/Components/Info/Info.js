import React from "react";
import Logo from "../../../../../assets/LOGO.png";
import './Info.css'

class Info extends React.Component{
    render(){
        return(
            <div className="container-fluid" id="home-info-con">
                <div className="row" id="home-info-row">
                    <div className="col-md-7 col-lg-5" id="home-info-col">
                        <img className="home-info-logo" src={Logo}/>
                        <p id="home-info-description">اولین و بزرگترین وبسایت سفارش آنلاین غذا در دانشگاه تهران</p>
                    </div>
                </div>
            </div>
        );
    }
}


export default Info