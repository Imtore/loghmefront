import React from 'react'
import './FoodPartyCard.css'
import translate from "../../../../Utilities/translate";
import UnavailableButton from "../../../Restaurant/Components/UnavailableButton/UnavailableButton";
import FoodPartyModal from "../FoodPartyModal/FoodPartyModal";
import ButtonLoading from "../../../../Components/ButtonLoading/ButtonLoading";


class FoodPartyCard extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            seen:false,
            cartIsLoading:props.cartIsLoading
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.cartIsLoading !== this.props.cartIsLoading){
            this.setState({cartIsLoading :nextProps.cartIsLoading});
        }
    }
    togglePop(event){
        if(event.target.id==="md1"){
            this.setState({
                seen: false
            });
        }
        else if(event.target.id==="fpc1"){
            this.setState({
                seen: true
            });
        }
    };

    render(){

        let rate = this.props.info.popularity;
        if(rate!==undefined){
            rate = translate(rate.toString());
        }

        const button = ()=>{
            if(this.props.info.count>0){
                return(
                    <button type="button" className="btn btn-info home-fp-add" id="notfpc"
                            onClick={()=>{
                                console.log("here");
                                this.props.onAddClick({name:this.props.info.name, price:this.props.info.newPrice, restaurantId:this.props.info.restaurantId, count:1})
                            }}>خرید</button>);
            }
            //TODO: change style, this is temporary
            return (<UnavailableButton/>);
        }

        return(
            <div className="card col-md-2 col-lg-2 text-center stretched-link home-fp-card" id="fpc1" onClick={(event)=>{this.togglePop(event)}}>
                {this.state.seen ? <FoodPartyModal info={this.props.info} cartIsLoading={this.state.cartIsLoading}
                                              onClick={(info)=>
                                              {
                                                  if(info.count!==0){
                                                      this.setState({seen:false})
                                                  }
                                                  this.props.onAddClick(info);
                                              }}
                                              restaurantName={this.props.info.restaurantName} restaurantId={this.props.info.restaurantId} toggle={(event)=>{this.togglePop(event)}}/> : null}

                <div className="card-body home-fp-card-body" >
                    <div className="home-fp-content">
                        <div className="row home-fp-content-name-rate-pic">
                            <div className="col-4">
                                <img className="home-fp-img" src={this.props.info.imageURL}/>
                            </div>
                            <div className="col-8">
                                <span className="home-fp-title">{this.props.info.name} </span>
                                <div className="home-fp-rate-sec">
                                    <p className="home-fp-rate">{rate}</p>
                                    <p className="home-fp-star">&#9733;</p>
                                </div>
                            </div>
                        </div>
                        <div className="home-fp-price-sec">
                            <span className="home-fp-price" id="old">{translate(this.props.info.price.toString())}</span>
                            <span className="home-fp-price" id="new">{translate(this.props.info.newPrice.toString())}</span>
                        </div>
                        <div className="home-fp-submit-sec">
                            <span className="home-fp-available-count">موجودی:{translate(this.props.info.count.toString())}</span>
                            {this.state.cartIsLoading?<ButtonLoading className="addtocart-disabled"/>:button()}
                        </div>
                    </div>
                    <hr className="home-fp-line"/>
                    <span className="home-fp-restaurant">{this.props.info.restaurantName}</span>
                </div>
            </div>
        );
    }
}

export default FoodPartyCard