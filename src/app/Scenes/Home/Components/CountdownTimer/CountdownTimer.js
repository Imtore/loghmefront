import React from 'react'
import './CountdownTimer.css'
import translate from "../../../../Utilities/translate";


class CountdownTimer extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            minutes:this.props.minutes,
            seconds:this.props.seconds
        }
    }


    componentDidMount() {
        this.myInterval = setInterval(() => {
            const minutes = this.state.minutes
            const seconds  = this.state.seconds

            if (seconds > 0) {
                this.setState(({ seconds }) => ({
                    seconds: seconds - 1
                }))
            }
            if (seconds === 0) {
                if (minutes === 0) {
                    clearInterval(this.myInterval)
                    this.props.onDone()
                } else {
                    this.setState(({ minutes }) => ({
                        minutes: minutes - 1,
                        seconds: 59
                    }))
                }
            }
        }, 1000)
    }

    componentWillUnmount() {
        clearInterval(this.myInterval)
    }

    render() {
        const minutes =this.state.minutes
        const seconds  = this.state.seconds
        return (
            <div className="home-counter-box">
                { minutes === 0 && seconds === 0 ? <span className="home-counter-text" id="off">اتمام زمان!!</span> : <span className="home-counter-text" id={"on"}>زمان باقی‌مانده: {translate(minutes.toString())}:{seconds < 10 ? `۰${translate(seconds.toString())}` : translate(seconds.toString())}</span>  }
            </div>
        )
    }
}

export default CountdownTimer