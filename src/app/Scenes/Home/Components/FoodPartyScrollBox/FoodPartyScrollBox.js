import React from "react";
import './FoodPartyScrollBox.css'
import FoodPartyCard from "../FoodPartyCard/FoodPartyCard";



class FoodPartyScrollBox extends React.Component{
    constructor(props) {
        super(props);
        this.state= {
            cartIsLoading : this.props.cartIsLoading
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.cartIsLoading !== this.props.cartIsLoading){
            this.setState({cartIsLoading :nextProps.cartIsLoading});
        }
    }

    refresh(){
        this.props.refresh()
    }


    addFoodPartyItem(info){
        this.props.onAddClick(info)

        const timer = setTimeout(() => {
            this.props.onClickUpdate()
        }, 2000);
        return () => clearTimeout(timer);
    }

    render(){

        let items ;
        if(this.props.partyFoods){
            items = this.props.partyFoods.map((food, index)=>{
                return(
                    <FoodPartyCard key={index} cartIsLoading = {this.state.cartIsLoading}  info={food} onAddClick={(info)=>this.addFoodPartyItem(info)} />
                );
            });
        }
        return(
            <div className="home-foodParty">
                <div className="home-scroll-counter-sec">
                    <div id="home-counter"></div>
                    {this.props.timeOut ? <button className="home-foodParty-refresh" onClick={()=>{this.refresh()}}>دوباره؟</button> : null}
                </div>
                <div className="row home-scroll-menu-row">
                    <div className="col-12 home-scroll-menu-col">
                        <div className="home-scroll-menu">
                            {this.props.timeOut ? null : items}
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}


export default FoodPartyScrollBox