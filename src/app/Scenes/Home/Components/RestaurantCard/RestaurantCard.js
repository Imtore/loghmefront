import React from 'react'
import './RestaurantCard.css'
import {Link} from 'react-router-dom'

class RestaurantCard extends React.Component{

    render(){
        const url = '/restaurant/'+this.props.info.id
        return(
            <div className="card col-md-3 col-lg-2 text-center home-restaurant-card">
                <img className="home-restaurant-image" src={this.props.info.logoURL}/>
                <p className="home-restaurant-name" >{this.props.info.name}</p>
                <Link className="btn btn-warning home-show-menu home-warning" to={url} >نمایش منو</Link>
            </div>
        );
    }
}



export default RestaurantCard