import React from 'react';
import './SearchBox.css';

class SearchBox extends React.Component
{
    constructor(props) {
        super(props);
        this.state = {
            foodName: '',
            restaurantName: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = ({ target }) => {
        this.setState({ [target.name]: target.value });
    };

    handleSubmit(event) {
        event.preventDefault();

        var condition1 = this.state.foodName === '' && this.state.restaurantName ==='' ;
        if(condition1)
        {
            alert('At least one fields need to be filled');
        }
        else
        {
            alert('We are searching!')
            this.props.onSearch(this.state.foodName, this.state.restaurantName)
            //TODO: call props with args
        }
        this.setState({foodName:'', restaurantName:''})
    }

    render() {
        return(
            <div id='home-serach-box-div'>
                <form className='home-sb-form' onSubmit={this.handleSubmit}>
                    <div  className="searchbox-form-row">
                        <div className="home-sp-input-section">
                            <div className="home-sb-item" id='home-sb-foodname-div' >
                                <input type="text" name="foodName" value={this.state.foodName} onChange={this.handleChange} className='form-control home-serachbox'  placeholder="نام غذا"/>
                            </div>
                            <div className="home-sb-item" >
                                <input type="text" name="restaurantName" value={this.state.restaurantName} onChange={this.handleChange} className='form-control home-serachbox' placeholder="نام رستوران"/>
                            </div>
                        </div>
                        <div className="home-sb-button-div" >
                        <button type="submit" className="btn btn-info home-sb-button">جست‌و‌جو</button>
                        </div>
                    </div>
                </form>
            </div>


        );
    }

}



export default SearchBox;