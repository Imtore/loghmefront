import React from "react";
import './FoodPartyModal.css'
import translate from "../../../../Utilities/translate";
import UnavailableButton from "../../../Restaurant/Components/UnavailableButton/UnavailableButton";
import ButtonLoading from "../../../../Components/ButtonLoading/ButtonLoading";


class FoodPartyModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 0,
            cartIsLoading : props.cartIsLoading
        }
    }

    increaseCount(){
        if(this.state.count+1<=this.props.info.count){
            this.setState({count:this.state.count+1})
        }
    }
    omponentWillReceiveProps(nextProps) {
        if (nextProps.cartIsLoading !== this.props.cartIsLoading) {
            this.setState({cartIsLoading: nextProps.cartIsLoading});
        }
    }

    decreaseCount(){
        if(this.state.count!==0){
            this.setState({count:this.state.count-1})
        }
    }

    render() {
        let rate = this.props.info.popularity;
        if(rate){
            rate = translate(rate.toString());
        }

        let price = this.props.info.price;
        if(price){
            price = translate(price.toString());
        }


        let newPrice = this.props.info.newPrice;
        if(newPrice){
            newPrice = translate(newPrice.toString());
        }

        const button = ()=>{
            if(this.props.info.count>0){
                return(
                    <button type="button" className="btn btn-info rst-info"
                    onClick={()=>{
                        this.props.onClick({name:this.props.info.name, price:this.props.info.newPrice, restaurantId:this.props.restaurantId, count:this.state.count})
                    }}>افزودن به سبد خرید</button>);
            }
            //TODO: change style, this is temporary
            return (<UnavailableButton/>);
        }

        return (
            <div className="modal foodParty-modal" id="md1" onClick={this.props.toggle}>
                <div className="modal-content foodParty-modal">
                    <div className="row home-fp-modal-row">
                        <div className="card-body home-fp-modal-body">
                            <p id="home-fp-modal-restaurant-name">{this.props.restaurantName}</p>
                            <div className="rst-food-modal-content">
                                <div className="row home-fp-modal-content-row">
                                    <div className="col-4">
                                        <img className="home-fp-modal-img" src={this.props.info.imageURL}/>
                                    </div>
                                    <div className="col-8">
                                        <div className="home-fp-modal-name-population">
                                            <p className="home-fp-modal-title">{this.props.info.name} </p>
                                            <p className="home-fp-modal-rate">{rate}</p>
                                            <p className="home-fp-modal-star">&#9733;</p>
                                        </div>
                                        <p className="food-description">{this.props.info.description}</p>
                                        {/*TODO: this section needs style*/}
                                        <div>
                                            <p className="card-subtitle text-muted">{newPrice} تومان</p>
                                            <p>{price}</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-8 rst-food-modal-order-row">
                                        <div className="rst-add-panel">
                                            <span className="flaticon-plus rst" onClick={()=>{this.increaseCount()}}></span>
                                            <p className="rst-cart-food-count">{translate(this.state.count.toString())}</p>
                                            <i className="flaticon-minus rst" onClick={()=>{this.decreaseCount()}}></i>
                                        </div>
                                        {this.state.cartIsLoading?<ButtonLoading className={"addtocart-disabled"}/>:button()}
                                    </div>
                                    <div className="col-4">
                                        {/*<p>{add availability of food}</p>*/}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


export default FoodPartyModal