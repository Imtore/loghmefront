import React from "react";
import axios from "axios";
import AuthService from './../../Services/auth.service'
import {Link} from "react-router-dom";

class SignUpForm extends React.Component
{
    constructor(props) {
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            email:'',
            phoneNumber:'',
            password:'',
            passwordRepeat:''

        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = ({ target }) => {
        this.setState({ [target.name]: target.value });
    };

    handleSubmit(event) {
        event.preventDefault();
        var condition1= this.state.password === this.state.passwordRepeat;
        var condition2 = this.state.email !== '' && this.state.password !=='' && this.state.firstName !== '' && this.state.lastName !=='' && this.state.passwordRepeat !== '' ;
        if(condition2 && condition1) {
            AuthService.register(this.state.firstName, this.state.lastName, this.state.phoneNumber, this.state.email, this.state.password)
                .then((response)=> {
                    alert("You're now a member, welcome "+ this.state.firstName);
                    AuthService.login(this.state.email, this.state.password).then(
                        () => {
                            this.props.redirect()
                        }
                    )
                })
                .catch((error) =>{
                    alert(error.response.data.message);
                })

        }
        else if(!condition2)
        {
            alert('All fields are essential');
            this.setState(prevState =>({firstName: '',lastName: '',email:'',phoneNumber:'', password:'',passwordRepeat:''}));
        }
        else
        {
            alert("Passwords don't match");
            this.setState(prevState =>({firstName: '',lastName: '',email:'',phoneNumber:'', password:'',passwordRepeat:''}));
        }


    }
    render()
    {
        return(
            <form onSubmit={this.handleSubmit} className='signup-form'>
                <div className="signup-form-row">
                    <div className="signup-form-group signup-form-group-addition col-md-4">
                        <label htmlFor="inputFirstName" className='signup-label'>نام</label>
                        <input type="text" name='firstName' value={this.state.firstName} onChange={this.handleChange} className="form-control signup-input" id="signup-inputFirstName" placeholder="نام"/>
                    </div>
                    <div className="signup-form-group col-md-6">
                        <label htmlFor="inputLastName" className='signup-label'>نام خانوادگی</label>
                        <input type="text" name='lastName' value={this.state.lastName} onChange={this.handleChange} className="form-control signup-input" id="signup-inputLastName" placeholder="نام خانوادگی"/>
                    </div>
                </div>
                <div className="signup-form-group">
                    <label htmlFor="inputEmail" className='signup-label'>ایمیل</label>
                    <input type="email" name='email' value={this.state.email} onChange={this.handleChange} className="form-control signup-input"  placeholder="ایمیل" />
                </div>
                <div className="signup-form-group col-md-5 signup-phone-number">
                    <label htmlFor="phone" className='signup-label'>شماره موبایل</label>
                    <input type="tel" name='phoneNumber' value={this.state.phoneNumber} onChange={this.handleChange} className="form-control signup-input" id="signup-phone" placeholder="۰۹**-***-****"
                           pattern="[0-9]{11}" />
                </div>
                <div className="signup-form-group col-md-7 signup-password1">
                    <label htmlFor="inputPassword1" className='signup-label'>رمز عبور</label>
                    <input type="password" name='password' value={this.state.password} onChange={this.handleChange} className="form-control signup-input" id="signup-inputPassword1" placeholder="رمز عبور"/>
                </div>
                <div className="signup-form-group col-md-7 signup-password2">
                    <label htmlFor="inputPassword2" className='signup-label'>تکرار رمز عبور</label>
                    <input type="password" name='passwordRepeat' value={this.state.passwordRepeat} onChange={this.handleChange} className="form-control signup-input" id="signup-inputPassword2"
                           placeholder="رمز عبور خود را تکرار کنید."/>
                </div>
                <button type="submit" className="btn btn-info" id="signup-confirm">ثبت نام</button>
                <Link to='/login' >ورود</Link>
            </form>
        );
    }


}


function FormItem (props) {
    return (
            <div className={"signup-form-group "+ props.colValue}>
                <label htmlFor={props.htmlFor} className='signup-label'>{props.labelValue}</label>
                <input type={props.type} name={props.name} value={props.val} onChange={props.handlechange} className="form-control signup-input" id={props.id} placeholder={props.placeHolder}/>
            </div>
    );
}
// <FormItem className='signup-form-group-addition col-md-4' htmlFor="inputFirstName" labelValue='نام' type='text' name='firstName' value={this.state.firstName} ChangeHandler={this.handleChange} id='signup-inputFirstName' placeHolder="نام"/>

export default SignUpForm;