import React from "react";
import './SignUp.css';
import Logo from '../../../assets/LOGO.png';
import SignUpForm from './SignUpForm'
import AuthService from './../../Services/auth.service'
import {Link, Redirect} from "react-router-dom";


class SignUp extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            redirectToHome: false
        }

    }

    signup(){
        this.setState({
            redirectToHome: true
        })
    }

    render()
    {
        const { redirectToHome } = this.state;

        if (redirectToHome || AuthService.getCurrentUser()) {
            return (
                <Redirect to={'/'}/>
            )
        }
        return (
            <div className="container-fluid" id='signup-page'>
                <WelcomeRow />
                <div className="row" id="signup-main-row">
                    <div className="col-md-5 col-lg-4" id="signup-main-col">
                        <SignUpForm redirect={()=>{this.signup()}}/>
                    </div>
                </div>
            </div>
        );
    }
}


function WelcomeRow() {
    return(
        <div className="row" id="signup-welcome-row">
            <div className="col-md-6 col-lg-5" id="signup-welcome-col">
                <img className="signup-logo" src={Logo}/>
                <p id="signup-description">اولین و بزرگترین سامانه سفارش غذا در دانشگاه تهران</p>
            </div>
        </div>
    );
}



export default SignUp;