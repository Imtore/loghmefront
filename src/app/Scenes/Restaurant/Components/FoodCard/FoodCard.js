import React from "react";
import './FoodCard.css'
import translate from "../../../../Utilities/translate";
import AddToCartButton from "../AddToCartButton/AddToCartButton";
import UnavailableButton from "../UnavailableButton/UnavailableButton";
import FoodModal from "../FoodModal/FoodModal";
import ButtonLoading from "../../../../Components/ButtonLoading/ButtonLoading";

//TODO: make css class and id names better
//TODO: improve style
class FoodCard extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            seen:false,
            cartIsLoading:props.cartIsLoading
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.cartIsLoading !== this.props.cartIsLoading){
            this.setState({cartIsLoading :nextProps.cartIsLoading});
        }
    }

    togglePop(event){
        if(event.target.id==="md1"){
            this.setState({
                seen: false
            });
        }
        else if(event.target.id==="fc1"){
            this.setState({
                seen: true
            });
        }
    };

    render() {

        let rate = this.props.info.popularity;
        if(rate!==undefined){
            rate = translate(rate.toString());
        }
        let price = this.props.info.price;
        if(price){
            price = translate(price.toString());
        }
        const button = ()=>{
            if(this.props.available){
                return(<AddToCartButton onClick={()=>this.props.onCardClick(this.props.i)} colorClass={'btn-warning rst-warning'}/>);
            }
            return (<UnavailableButton/>);
        }

        return (
            <div className="card col-md-3 col-lg-2 text-center stretched-link rst-food-card" id="fc1" onClick={(event)=>{this.togglePop(event)}}>
                {this.state.seen ? <FoodModal info={this.props.info}
                                              onClick={(info)=>
                                              {
                                                  if(info.count!==0){
                                                      this.setState({seen:false})
                                                  }
                                                  this.props.onModalClick(info);
                                              }}
                                              restaurantName={this.props.restaurantName} restaurantId={this.props.restaurantId} available={this.props.available} toggle={(event)=>{this.togglePop(event)}}/> : null}
                <img className="rst-card-img-top" src={this.props.info.imageURL}/>
                    <div className="card-body rst-food-card-body" >
                        <div className="rst-food-name-population">
                            {/*<div className="rst-food-title">*/}
                            {/*    <span>{this.props.info.name} </span>*/}
                            {/*</div>*/}
                            <span className="rst-food-title">{this.props.info.name}</span>
                            <p className="rst-food-rate">{rate}</p>
                            <p className="rst-star">&#9733;</p>
                        </div>
                        <p className="rst-card-subtitle text-muted food-price">{price} تومان</p>
                        {this.state.cartIsLoading?<ButtonLoading className="rst-food-addtocart-disabled"/>:button()}
                    </div>
            </div>
        );
    }
}

export default FoodCard