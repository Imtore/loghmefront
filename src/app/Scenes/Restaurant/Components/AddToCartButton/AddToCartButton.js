import React from "react";
import './AddToCardButton.css'

function AddToCartButton(props){
    const className = "btn " + props.colorClass +" rst-add-to-cart";
    return(
        <button type="button" className={className} onClick={props.onClick}>افزودن به سبد خرید</button>
    );
}

export default AddToCartButton