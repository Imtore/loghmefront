import React from "react";
import './UnavailableButton.css'

function UnavailableButton(){
    return(
        <button type="button" className="btn btn-warning rst-unavailable">ناموجود</button>
    );
}

export default UnavailableButton