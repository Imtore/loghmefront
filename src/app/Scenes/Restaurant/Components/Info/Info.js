import React from "react";
import "./Info.css"


class Info extends React.Component{
    render(){
        return(
            <div className="container-fluid" id="rst-restaurant-info-con">
                <div className="row" id="rst-restaurant-info-pic">
                    <div className="col-3" id="rst-restaurant-pic">
                        <img className="rst-center-cropped-res-pic"
                             src={this.props.resImageURL}/>
                    </div>
                </div>
                <div className="row" id="rst-restaurant-info-name">
                    <div className="col-6" id="rst-restaurant-name-col">
                        <p id="rst-restaurant-name">{this.props.resName}</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default Info