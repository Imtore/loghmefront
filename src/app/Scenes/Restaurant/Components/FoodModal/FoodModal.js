import React from "react";
import './FoodModal.css'
import translate from "../../../../Utilities/translate";
import AddToCartButton from "../AddToCartButton/AddToCartButton";
import UnavailableButton from "../UnavailableButton/UnavailableButton";

class FoodModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 0
        }
    }

    increaseCount(){
        this.setState({count:this.state.count+1})
    }

    decreaseCount(){
        if(this.state.count!==0){
            this.setState({count:this.state.count-1})
        }
    }

    render() {
        let rate = this.props.info.popularity;
        if(rate){
            rate = translate(rate.toString());
        }

        let price = this.props.info.price;
        if(price){
            price = translate(price.toString());
        }

        const button = ()=>{
            if(this.props.available){
                return(<AddToCartButton
                    onClick={()=>{
                            this.props.onClick({name:this.props.info.name, price:this.props.info.price, restaurantId:this.props.restaurantId, count:this.state.count})
                        }
                    }
                    colorClass={'btn-info rst-info'}/>);
            }
            return (<UnavailableButton/>);
        }

        return (
            <div className="modal food-modal" id="md1" onClick={this.props.toggle}>
                <div className="modal-content food-modal">
                    <div className="row" id="rst-food-modal-row" >
                        <div className="card text-center rst-food-modal-card" >
                            <div className="card-body rst-food-modal-body">
                                <p id="rst-owner">{this.props.restaurantName}</p>
                                <div className="rst-food-modal-content">
                                    <div className="row rst-food-modal-content-row">
                                        <div className="col-4">
                                            <img className="rst-modal-img" src={this.props.info.imageURL}/>
                                        </div>
                                        <div className="col-8">
                                            <div className="rst-food-name-population">
                                                <p className="rst-food-title">{this.props.info.name} </p>
                                                <p className="rst-food-rate">{rate}</p>
                                                <p className="rst-star">&#9733;</p>
                                            </div>
                                            <p className="food-description">{this.props.info.description}</p>
                                            <p className="card-subtitle text-muted food-price">{price} تومان</p>
                                        </div>
                                    </div>
                                    <div className="row rst-food-modal-order-row">
                                        <div className="rst-add-panel">
                                            <span className="flaticon-plus rst" onClick={()=>{this.increaseCount()}}></span>
                                            <p className="rst-cart-food-count">{translate(this.state.count.toString())}</p>
                                            <i className="flaticon-minus rst" onClick={()=>{this.decreaseCount()}}></i>
                                        </div>
                                        {button()}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


export default FoodModal