import React from "react";
import './Restaurant.css'
import Info from "./Components/Info/Info";
import axios from 'axios';
import Cart from "../../Components/Cart/Cart";
import FoodCard from "./Components/FoodCard/FoodCard";
import Header from "../../Components/Header/Header";
import Loading from '../../Components/PageLoading/Loading';
import authHeader from "../../Services/authHeader";


function MenuHeader(){
    return(
        <div className="row rst-content-header-row">
            <div className="col-md-4 col-lg-3">
                <div className="row">
                    <div className="col">
                        <p id="rst-print-hide">منوی غذا</p>
                    </div>
                </div>
            </div>
            <div className="col-md-8 col-lg-9">
                <div className="row" id="rst-menu-header-row">
                    <div className="col-5" id="rst-menu-header-col">
                        <p id="rst-menu-header-text">منوی غذا</p>
                    </div>
                </div>
            </div>
        </div>
    );
}

class Restaurant extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            restaurant : {
                id: null,
                name: null,
                description: null,
                location: {
                    x: null,
                    y: null
                },
                menu:[{
                        food:{
                            name: null,
                            description: null,
                            popularity: null,
                            price: null,
                            imageURL: null
                        }
                    }
                ],
                foodPartyItems:[],
                logoURL: null,
                inFoodParty: null
            },
            pageIsLoading:false,
            cartIsLoading: props.cartIsLoading
        }
    }


    componentWillReceiveProps(nextProps){
        if(nextProps.cartIsLoading !== this.props.cartIsLoading){
            this.setState({cartIsLoading :nextProps.cartIsLoading});
        }
    }


    //TODO : add authHeader if needed
    getRestaurantInfo(restaurantId){
        axios.get('restaurants/' + restaurantId, {headers: authHeader()})
            .then((response)=> {
                const restaurant = response.data;
                this.setState({restaurant:restaurant , pageIsLoading:false});
                // console.log(response);
            })
            .catch(function (error) {
                // TODO: an error page for 400 that shows the message from back
                console.log(error);
            })
            .then(function () {
                //TODO: read about this
                //always executed
            });
    }



    componentDidMount() {
        this.props.setCart();
        this.setState({pageIsLoading:true})
        const { id } = this.props.match.params
        this.getRestaurantInfo(id);
        this.timerId = setInterval(
            () => {
                this.getRestaurantInfo(id);
            }
            , 5000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerId);
    }


    //TODO: don't need this, modify
    addToCartFromCard(i){
        const food = this.state.restaurant.menu[i];
        console.log(food.name)
        console.log(food.price)
        this.props.onAddClick({name:food.name, price:food.price, restaurantId:this.state.restaurant.id, count:1});
    }


    render(){
        const menu = this.state.restaurant.menu;
        let foods;
        // console.log(menu);
        if(menu){
            foods = menu.map((food, index)=>{
                return (
                    //TODO: fix available, doesn't need i and count
                    <FoodCard key={index} i={index} cartIsLoading={this.state.cartIsLoading} count={this.props.cart.cartItems[food.name]} info={food} available={true} restaurantName={this.state.restaurant.name} restaurantId={this.state.restaurant.id} onModalClick={(info)=>this.props.onAddClick(info)} onCardClick={(i)=>this.addToCartFromCard(i)}/>
                );
            });
        }

        return(
            <div>
                {this.state.pageIsLoading?<Loading />:null}
                <Header cartIsLoading ={this.state.cartIsLoading} cart={this.props.cart} exitLink={true} profileLink={true} onAddClick={(info)=>{this.props.onAddClick(info)}} onSubmitClick={()=>{this.props.onSubmitClick()}}/>
                <Info resName={this.state.restaurant.name} resImageURL={this.state.restaurant.logoURL}/>
                <div className="container-fluid" id="rst-cart-menu">
                    <MenuHeader/>
                    <div className="row rst-content-row">
                        <div className="col-md-4 col-lg-3">
                            <div className="row" id="rst-cart-row">
                                <div className="col">
                                    <Cart cartIsLoading ={this.state.cartIsLoading} info={this.props.cart} onAddClick={(info)=>{this.props.onAddClick(info)}} onSubmitClick={()=>{this.props.onSubmitClick()}} />
                                </div>
                            </div>
                        </div>
                        <div className="col-md-8 col-lg-9">
                            <div className="row" id="menu-row">
                                {foods}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Restaurant



