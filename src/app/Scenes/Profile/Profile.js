import React from 'react';
import './Profile.css';

import CreditTab from './CreditTab';
import OrderTab from './OrderTab';
import Tabs from "./Tabs";
import Loading from '../../Components/PageLoading/Loading';
import PopUp from './PopUp'
import phone from './ProfileImages/telephone.png';
import mail from './ProfileImages/mail.png';
import credit from './ProfileImages/credit-card.png';
import user from './ProfileImages/account.png';
import changeDigits from "../../Utilities/changeDigits";
import Header from "../../Components/Header/Header";
import authHeader from "../../Services/authHeader";


class Profile extends React.Component
{
    constructor(props) {
        super(props);
        this.state = {
            firstName : '',
            lastName : '',
            phoneNumber : '',
            email : '',
            credit: 0,
            orders: [],
            seen: false,
            toBeShownOrder: -1,
            pageIsLoading: false
        };
        //TODO call a function to get user and set user attributes
    }
    togglePop = () => {
        this.setState({
            seen: !this.state.seen
        });
    };

    changeShownOrder = (id) =>
    {
        this.setState({toBeShownOrder:id});
    };

    //TODO : authHeader is needed! change fetch to axios and add.
    fetchUserInfo() {
        const options = {
            headers: new Headers(authHeader()),
        };
        fetch('http://ie.etuts.ir:30180/Loghme/users', options)
            .then(resp => resp.json())
            .then((data) => {this.setState(prevState => ({ firstName : data.firstName , lastName: data.lastName, phoneNumber:data.phoneNumber,
                email: data.email, credit: data.credit,
                orders:data.orders, pageIsLoading: false}));});
        // console.log(this.state.orders)
        // let a = Array.from(this.state.orders);
        // this.setState({orders : a})
        // console.log(this.state.orders);

    }
    componentDidMount() {
        this.props.setCart()
        this.setState({pageIsLoading:true})
        this.fetchUserInfo();
        this.timerId = setInterval(()=>{
            this.fetchUserInfo()
        }, 5000)
        //alert(this.state.orders)
    }
    componentWillUnmount() {
        clearInterval(this.timerId);
    }

    render()
    {
        return (
            <div>
                {this.state.pageIsLoading?<Loading />:null}
                <Header cartIsLoading ={this.state.cartIsLoading} cart={this.props.cart} exitLink={true} profileLink={false} onAddClick={(info)=>{this.props.onAddClick(info)}} onSubmitClick={()=>{this.props.onSubmitClick()}}/>
                <div className="profile-body">
                    <UserInfo firstName={this.state.firstName} lastName={this.state.lastName} phoneNumber={this.state.phoneNumber}
                              email={this.state.email} credit={this.state.credit}/>
                    {this.state.seen ? <PopUp toggle={this.togglePop} order={this.state.orders[this.state.toBeShownOrder]} /> : null}
                    <ProfileTabs profile={this} orders={this.state.orders} toggle={this.togglePop} popUpValueHandler={this.changeShownOrder}/>
                </div>
            </div>

        );
    }
}

export default Profile
class UserInfo extends React.Component
{
    constructor(props) {
        super(props);
    }
    render()
    {
        return(

                <div className="container-fluid" id="profile-user-info-con">
                    <div id="profile-user-name">
                        <img alt="user logo "id="profile-logo" src={user}/>
                        <p id="profile-username">{this.props.firstName + " "+this.props.lastName}</p>
                    </div>
                    <div id="profile-user-info">
                        < UserInfoItem divId="profile-user-phone" imgId="profile-phone-logo" imgSrc={phone} valueId="profile-phone-number" itemValue={this.props.phoneNumber}/>
                        < UserInfoItem divId="profile-user-email" imgId="profile-mail-logo" imgSrc={mail} valueId="profile-email" itemValue={this.props.email}/>
                        < UserInfoItem divId="profile-user-credit" imgId="profile-credit-logo" imgSrc={credit} valueId="profile-credit" itemValue={changeDigits(this.props.credit)+' تومان'}/>
                    </div>
                </div>


        );
    }
}



function UserInfoItem(props)
{
    return(
        <div className="profile-user-info-item" id={props.divId}>
            <img alt="user info items logo" className="profile-info-logo" id={props.imgId} src={props.imgSrc}/>
            <p className="profile-info-item" id={props.valueId}>{props.itemValue}</p>
        </div>
    );
}

class ProfileTabs extends React.Component
{

    render() {
        return (
            <div className="container" id="profile-tabs-con">
                <Tabs defaultActiveTabIndex={0}>
                    <Tab linkClassName={'custom-link'} tabId='profile-credit-tab-button'  buttenText='افزایش اعتبار'>
                        <CreditTab profile={this.props.profile}/>
                    </Tab>
                    <Tab linkClassName={'custom-link'} tabId='profile-order-tab-button' buttenText='سفارش‌ها'>
                        <OrderTab orders = {this.props.orders} toggle={this.props.toggle} popUpValueHandler={this.props.popUpValueHandler}/>
                    </Tab>
                </Tabs>
            </div>
        );
    }
}

function Tab (props) {
    return (
        <li className={`profile-tab-link ${props.linkClassName} ${props.isActive ? 'active' : ''}`} id={props.tabId}>
            <a
                onClick={(event) => {
                    event.preventDefault();
                    props.onClick(props.tabIndex);
                }}>
                <span>{props.buttenText}</span>
            </a>
        </li>
    );
}



