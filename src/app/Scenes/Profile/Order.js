import React from "react";
import changeDigits from "../../Utilities/changeDigits";


class Order extends React.Component
{
    constructor(props) {
        super(props);
        // this.togglePop = this.togglePop().bind(this);

    }

    handleClick = () => {
        this.props.popUpValueHandler(this.props.order.id);
        this.props.toggle();
    };


    render() {
        var status = (this.props.order.status === 'FindingDelivery') ? 'در جستو‌جوی پیک'
            : (this.props.order.status === 'Delivering') ? 'پیک در مسیر' : 'مشاهده‌ی فاکتور';
        var className = "container profile-status-con profile-" + this.props.order.status + '-status';
        var id = this.props.order.id === 0 ? "profile-first-order" : "";
        let status_container;
        if(this.props.order.status === 'FindingDelivery' || this.props.order.status === 'Delivering')
        {
            status_container = <div className={className}>{status}</div>;
        }
        else
        {
            status_container = <button className={className + ' profile-see-details-button'} onClick={this.handleClick}>{status}</button>
        }

        return (
            <div className="row profile-order-row" id={id}>
                <div className="col-1 profile-order-number profile-order-info">{changeDigits(this.props.order.id + 1)}</div>
                <div className="col-6 profile-order-restaurant profile-order-info">{this.props.order.cart.restaurantName}</div>
                <div className="col-5 profile-order-status profile-order-info">
                    {status_container}
                </div>
            </div>
        );
    }
}








export default Order
