import React from 'react';
import './ProfileCredit.css';
import ButtonLoading from '../../Components/ButtonLoading/ButtonLoading';
import authHeader from "../../Services/authHeader";

class CreditTab extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            amount: undefined,
            isLoading:false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({amount: event.target.value});
    }

    handleSubmit(event) {
        event.preventDefault();
        var regex=/^[0-9]+$/;
        const valid = this.state.amount !== undefined && this.state.amount.match(regex);
        if(valid){
            this.setState({isLoading:true});
            this.submitForm();
        }
        else {
            alert("Input must only contain numbers");
            this.setState(prevState =>({amount:""}));
        }


    }

    //TODO : authHeader is needed! change fetch to axios and add.
    submitForm()
    {

        var url = 'http://ie.etuts.ir:30180/Loghme/users/credit?credit='+ this.state.amount;
        fetch(url, {
            method: 'PUT',
            headers: new Headers(authHeader())
        }).then(resp => resp.json())
            .then(data => {this.props.profile.setState(prevState => ({ credit : data} )); this.setState({isLoading:false})});
        this.setState(prevState =>({amount:""}));
    }

    render() {
        return (
            <div className="container" id="profile-credit-tab-con">
                <form onSubmit={this.handleSubmit} id="profile-credit-form">
                    <div id="profile-amount-form">
                        <input type="text" value={this.state.amount} onChange={this.handleChange} className='profile-input'name="credit" placeholder="میزان افزایش اعتبار" />
                    </div>
                    <div id="profile-button-div">
                        {this.state.isLoading?<ButtonLoading className="profile-increase-button profile-input" />:null}
                        {!this.state.isLoading?<input type="submit" value="افزایش" className="profile-increase-button profile-input"/>:null}
                    </div>
                </form>
            </div>
        );
    }
}

export default CreditTab
