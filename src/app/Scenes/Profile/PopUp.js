import React from "react";
import './PopUp.css';
import changeDigits from "../../Utilities/changeDigits";
class PopUp extends React.Component {

    constructor(props) {
        super(props);

    }
    handleClick = () => {
        this.props.toggle();
    };
    render() {
        // console.log(this.props.order);
        const itemCounts = this.props.order.cart.cartItems;
        const itemPrices = this.props.order.cart.cartItemPrices;
        let items;
        items = Object.keys(itemCounts).map((item, index)=>{
            const name = item;
            const count = itemCounts[name];
            const price = itemPrices[name];
            return (
                <tr>
                    <td>{changeDigits(index+1)}</td>
                    <td>{name}</td>
                    <td>{changeDigits(count)}</td>
                    <td>{changeDigits(price)}</td>
                </tr>
            );
        });
        return (
            <div className="modal profile-modal">
                <div className="profile-modal-content">
                    <span className="close" onClick={this.handleClick}>&times;   </span>
                    <div className="row" id="profile-restaurant-name-row">
                        <div className="col-lg-6 col-md-8" id='profile-restaurant-name-container'>
                            <p id='profile-restaurant-name'>{'رستوران '+this.props.order.cart.restaurantName}</p>
                        </div>
                    </div>
                    <div className="profile-line"></div>
                    <table className='profile-order-table'>
                        <tr>
                            <th className='profile-order-table' id='profile-number'>ردیف</th>
                            <th className='profile-order-table' id='profile-foodName'>نام غذا</th>
                            <th className='profile-order-table' id='profile-count'>تعداد</th>
                            <th className='profile-order-table' id='profile-price'>قیمت</th>
                        </tr>
                        {items}
                    </table>
                    <div className="row" id="profile-total-price-row">
                        <div className="col-lg-5 col-md-7" id='profile-restaurant-name-container'>
                            <p id='profile-total-amount'>{'جمع کل: '+ changeDigits(this.props.order.cart.total) + 'تومان'}</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


function TableRow(id,foodName,count,foodPrice)
{
    return(
        <tr>
            <td >{id}</td>
            <td >{foodName}</td>
            <td >{count}</td>
            <td >{foodPrice}</td>
        </tr>
    );
}




export default PopUp;
