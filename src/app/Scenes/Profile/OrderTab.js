import React from 'react';
import './ProfileOrder.css';
import Order from './Order'

//TODO: make orders scrollable

class  OrderTab extends React.Component {

    constructor(props) {
        super(props);

    }
    render() {
        var tab_height =this.props.orders.length * 8.5;
        return (
            <div className="container" id="profile-order-tab-con" style={{height: tab_height + 'vh'}}>
                {this.props.orders.map((order) =>{
                    return <Order order={order} toggle={this.props.toggle} popUpValueHandler={this.props.popUpValueHandler} />;
                })}


            </div>
        );
    }
}




export default OrderTab
