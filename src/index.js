import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './normalize.css'
import './bootstrap_css/bootstrap.css'
import './bootstrap_css/bootstrap-grid.css'
import * as serviceWorker from './serviceWorker';
import Footer from "./app/Components/Footer/Footer";
import axios from 'axios'
import App from "./app/App";
import { BrowserRouter } from 'react-router-dom';

axios.defaults.baseURL = 'http://ie.etuts.ir:30180/Loghme/'


ReactDOM.render(
<div>
    <BrowserRouter>
        <App/>
    </BrowserRouter>
    <Footer/>
</div>
      ,
  document.getElementById('root')
);



// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
